from django import forms 

from .models import PurchaseHistory, Services

class PurchaseForm(forms.ModelForm):
	class Meta:
		model = PurchaseHistory
		fields = '__all__'

class ServiceForm(forms.ModelForm):
	class Meta:
		model = Services
		fields = '__all__'

