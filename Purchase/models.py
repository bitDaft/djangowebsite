from django.db import models

# Create your models here.

from USER.models import cUser
from Product.models import Product

class Services(models.Model):
	service_type = models.IntegerField()
	service_name = models.CharField(max_length=255)
	def __str__(self):
		return str(self.service_name)

class PurchaseHistory(models.Model):
	user = models.ForeignKey(cUser,on_delete=models.CASCADE)
	product = models.ForeignKey(Services)
	def __str__(self):
		return str(self.id)



