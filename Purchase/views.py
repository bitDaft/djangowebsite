from django.shortcuts import render, redirect

# Create your views here.
from django.views.generic import CreateView
from .models import Services, PurchaseHistory
from .forms import ServiceForm, PurchaseForm

from django.core.urlresolvers import reverse


from django.views.generic import TemplateView,View

def mod_response(response):
	response["Cache-Control"] = "no-cache, no-store, must-revalidate" # HTTP 1.1.
	response["Pragma"] = "no-cache" # HTTP 1.0.
	response["Expires"] = "0" # Proxies.
	return response

class ShowServiceView(TemplateView):
	template_name="Purchase/service.html"

class PurchaseProduct(CreateView):
	model = PurchaseHistory
	form_class = PurchaseForm

	def get(self,request):
		return mod_response(redirect(reverse('Home:home')))
	

	# includes :
	# making the Purchase 
	# confirming the purchase
	# generation of the purchase history entry
	# generation of reciept
	# conversion and option to downlaod reciept as pdf
	# emailing of the reciept

	# look at universal payment interface, 
	# email sending, pdf generation

		
				
				 	






