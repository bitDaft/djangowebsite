from django.conf.urls import include, url
from django.contrib.auth import views as auth_views
from django.views.generic import TemplateView

from .views import ShowServiceView,PurchaseProduct

urlpatterns = [
    url(r"^services/$",ShowServiceView.as_view(),name='services'),
    url(r"^success_purchase/$",PurchaseProduct.as_view(),name='success_purchase'),
]
