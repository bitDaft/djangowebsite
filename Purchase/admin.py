from django.contrib import admin

# Register your models here.
from .models import Services, PurchaseHistory


admin.site.register(Services)
admin.site.register(PurchaseHistory)