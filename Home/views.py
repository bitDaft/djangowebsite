from django.shortcuts import render, redirect

from django.views.generic import View, FormView
from Product.models import Product
from django.core.urlresolvers import reverse
from .models import PointOfInterest
# Create your views here.
from .forms import ff
from emails.forms import MailsForm

def mod_response(response):
    response["Cache-Control"] = "no-cache, no-store, must-revalidate" # HTTP 1.1.
    response["Pragma"] = "no-cache" # HTTP 1.0.
    response["Expires"] = "0" # Proxies.
    return response

class HomePage(View):
    template_name="Home/home2.html"
    def get(self,request):
        if request.user.is_authenticated():
            if request.user.is_superuser:
                return mod_response(redirect('/admin'))
        context = {"contact":MailsForm(),'form':ff()}
        return mod_response(render(request,self.template_name,context))

    def post(self,request):
        request.session['yes_mail'] = True
        request.session['mail_type'] = 0
        request.session['reciever'] = "crayoneater2121@gmail.com"
        request.session['subject'] = "[LootMarket] Contact by user to admin"
        request.session['sender'] = request.POST.get('sender')
        request.session['message'] = request.POST.get('message')
        return mod_response(redirect(reverse('emails:newmail')))


