from django.conf.urls import include, url

from django.conf.urls.static import static
from django.conf import settings
from django.contrib.staticfiles.urls import staticfiles_urlpatterns


from .views import HomePage
urlpatterns = [
    url(r"^$",HomePage.as_view(),name='home'),
]

