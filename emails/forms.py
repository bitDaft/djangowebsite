from django import forms

from django.forms.widgets import CheckboxSelectMultiple

from .models import mails
from USER.models import cUser 


# class ContactsForm(forms.ModelForm):
# 	class Meta:
# 		model = Contacts
# 		fields = '__all__'

class MailsForm(forms.ModelForm):
	class Meta:
		model = mails
		fields = ['sender','message']

	def save(self, commit=True):
		mail = super(MailsForm, self).save(commit=False)
		if commit:
			mail.mail_type = self.data.get('mail_type')
			mail.reciever = self.data.get('reciever')
			mail.subject = self.data.get('subject')
			mail.save()
		return mail

	def __init__(self, *args, **kwargs):    
		super(MailsForm, self).__init__(*args, **kwargs)
		self.fields["sender"].label = 'Email address'


