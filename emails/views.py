from django.shortcuts import render, redirect
from django.core.mail import send_mail
from django.core.urlresolvers import reverse
from django.views.generic import CreateView, DetailView, View
from .forms import MailsForm

from .models import mails
from USER.models import cUser 
from Product.models import Product

def mod_response(response):
    response["Cache-Control"] = "no-cache, no-store, must-revalidate" # HTTP 1.1.
    response["Pragma"] = "no-cache" # HTTP 1.0.
    response["Expires"] = "0" # Proxies.
    return response

class ContactView(View):
	def get(self,request):
		if not ('yes_mail' in request.session and request.session['yes_mail']):
			return mod_response(redirect(reverse('Home:home')))
		typ = request.session['mail_type']
		f4 = request.session['reciever']
		f3 = request.session['sender']
		f2 = request.session['message']
		f1 = request.session['subject']
		send_mail(f1,f2,f3,[f4])
		obj = mails.objects.create(mail_type=typ,subject=f1,message=f2,sender=f3,reciever=f4)
		obj.save()
		del request.session['sender']
		del request.session['message']
		del request.session['reciever']
		del request.session['subject']
		del request.session['mail_type']
		del request.session['yes_mail']
		return mod_response(redirect(reverse('Home:home')))

	def post(self,request,p1,p2,p3,p4):
		return mod_response(redirect(reverse('Home:home')))







