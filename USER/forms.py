from django import forms
from django.conf import settings
from django.contrib.auth.forms import UserCreationForm, UserChangeForm

from .models import cUser, ActivationKey

from django.utils.crypto import get_random_string
from django.core.mail import send_mail
import hashlib




def generate_activation_key(username):
    chars = 'abcdefghijklmnopqrstuvwxyz0123456789!@#$%^&*(-_=+)'
    secret_key = get_random_string(20, chars)
    return hashlib.sha256((secret_key + username).encode('utf-8')).hexdigest()

def send_activation_mail(cryptstr,uname):
	send_mail("[LootMarket] Activate your new account","Welcome,\n\nThank you for registering with us.\n\nTo activate your newly created account please click the link below, if the link cannot be clicked, copy paste it into your browser url bar.\n\nhttp://bitDaft.pythonanywhere.com/activate/"+cryptstr+"\n\nWe hope to see you around!!\n\nRegards,\nLootMarket",settings.EMAIL_HOST_USER,[uname])




class cUserCreationForm(UserCreationForm):
	def __init__(self, *args, **kwargs):
	    super(cUserCreationForm, self).__init__(*args, **kwargs)
	    self.fields['first_name'].label = "Full Name"

	UserCreationForm.password2 = None
	class Meta(UserCreationForm.Meta):
		model = cUser
		fields = ['first_name','email','password1',]
		# fields = '__all__'

	def clean_password2(self):
		password1 = self.cleaned_data.get("password1")
		if password1:
			raise forms.ValidationError(
				self.error_messages['password_empty'],
				code='password_empty',
			)
		return password1

		
	def save(self, commit=True):
		user = super(cUserCreationForm, self).save(commit=False)
		user.is_active=False
		if commit:
			user.save()
			cryptstr = generate_activation_key(user.username)
			key = ActivationKey.objects.create(user=user,hash_key=cryptstr)
			key.save()
			send_activation_mail(cryptstr,user.email)
		return user

class cUserChangeForm(UserChangeForm):
	UserChangeForm.password = None
	class Meta:
		model = cUser
		fields = ['email','first_name']

	def __init__(self, *args, **kwargs):
	    super(cUserChangeForm, self).__init__(*args, **kwargs)
	    self.fields['first_name'].label = "Full Name"






