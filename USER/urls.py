from django.conf.urls import include, url
from django.contrib.auth import views as auth_views
from django.views.generic import TemplateView
from django.conf import settings

from .views import LoginUser, ProfileView, RegisterUser, EditUser, DeleteUser,  ChangePasswordUser, activation

from django.views.generic import TemplateView 

urlpatterns = [
    # TODO create a custom view for profile to check whether superuser access
    url(r'^login/$',LoginUser.as_view(),name="login"),
    url(r'^logout/$',auth_views.logout,{'next_page': settings.LOGOUT_REDIRECT_URL},name="logout"),
    url(r'^register/$',RegisterUser.as_view(),name='register'),
    url(r'^activate/(?P<key>.+)/$',activation,name="activate"),
    url(r'^activation_success/$',TemplateView.as_view(template_name="activation_success.html"),name="activate_success"),
    
    url(r'^profile/$',ProfileView.as_view(),name="profile"),
    url(r'^changepassword/$',ChangePasswordUser.as_view(),name="changepassword"),
    url(r'^editprofile/$',EditUser.as_view(),name="editprofile"),
    
    url(r'^delete/$',DeleteUser.as_view(),name='delete'),

]
    



