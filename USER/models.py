from django.db import models

from django.contrib.auth.models import AbstractUser

# Create your models here.

class cUser(AbstractUser):
	#add additional fields here or use a proxy model
	
	USERNAME_FIELD = 'email'
	REQUIRED_FIELDS = ['first_name']
	def __str__(self):
		return self.first_name

cUser._meta.get_field('email')._unique = True
cUser._meta.get_field('username')._unique = False

class ActivationKey(models.Model):
	user = models.OneToOneField(cUser,related_name="key",blank=False,null=False,on_delete=models.CASCADE)
	hash_key = models.CharField(max_length=200,blank=False,null=False)
	def __str__(self):
		return str(self.user.email)



