import urllib
import urllib2
import json

from django.conf import settings

from django.shortcuts import render, redirect
from django.core.urlresolvers import reverse

from django.views.generic import View, TemplateView, CreateView, UpdateView, DeleteView, ListView, DetailView
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth import authenticate,login

from django.contrib.auth import update_session_auth_hash
from django.contrib.auth.forms import PasswordChangeForm

from .forms import cUserCreationForm, cUserChangeForm
from .models import cUser, ActivationKey
from Product.models import Product 

# Create your views here.

def mod_response(response):
	response["Cache-Control"] = "no-cache, no-store, must-revalidate" # HTTP 1.1.
	response["Pragma"] = "no-cache" # HTTP 1.0.
	response["Expires"] = "0" # Proxies.
	return response

class RegisterUser(CreateView):
	template_name = 'USER/login.html'
	model = cUser
	form_class = cUserCreationForm
	success_url = '/'
	def get(self,request):
		if request.user.is_authenticated():
			return mod_response(redirect(reverse('Home:home')))
		else:
			return mod_response(super(RegisterUser,self).get(self,request))
	def post(self,request):
		form = cUserCreationForm(request.POST)
		obj = cUser.objects.filter(email=request.POST.get('email')).first()
		if obj and not obj.is_active:
			obj.delete()
		elif obj and obj.is_active:
			return mod_response(redirect(reverse('Home:home')))
		return_url = request.GET.get('returnto')
		print(return_url,"/"*30)
		if return_url:
			print('*'*30)
			request.session['reg_return_url'] = return_url
			self.success_url = "/login"
		return mod_response(super(RegisterUser,self).post(self,request))

class LoginUser(View):
	def get(self,request):
		template_name = 'USER/login.html'
		context = {'form':AuthenticationForm(),'error':None,'ff':'log'}
		if request.user.is_authenticated():
			return mod_response(redirect(reverse('Home:home')))
		return mod_response(render(request,template_name,context))

	def post(self,request):
		if request.user.is_authenticated():
			return mod_response(redirect(reverse('Home:home')))
		f1 = request.POST.get('username')
		f2 = request.POST.get('password')
		user = authenticate(username=f1,password=f2)
		if user:
			if user.is_active:
				recaptcha_response = request.POST.get('g-recaptcha-response')
				url = 'https://www.google.com/recaptcha/api/siteverify'
				values = {
				'secret': settings.GOOGLE_RECAPTCHA_SECRET_KEY,
				'response': recaptcha_response
				}
				data = urllib.urlencode(values)
				req = urllib2.Request(url, data)
				response = urllib2.urlopen(req)
				result = json.load(response)
				if True and result['success']:
					login(request,user)
					callback_url = request.GET.get('returnto')
					if 'reg_return_url' in request.session:
						callback_url = request.session['reg_return_url']
						del request.session['reg_return_url']
					if callback_url:
						return mod_response(redirect(callback_url))
					return mod_response(redirect(reverse('Home:home')))
				else:
					print("//"*20,"Invalid reCaptcha")
		template_name = 'USER/login.html'
		context = {'form':AuthenticationForm(),'error':'Email or password incorrect!!'}
		return mod_response(render(request,template_name,context))

class ProfileView(ListView):
	template_name = "USER/profile.html"
	model = Product
	context_object_name = 'object'
	
	def get(self,request):
		if request.user.is_authenticated():
			if request.user.is_superuser:
				return mod_response(redirect(reverse('Home:home')))
			return mod_response(super(ProfileView,self).get(self,request))
		return mod_response(redirect(reverse('Home:home')))

	def get_queryset(self):
		rval = super(ProfileView,self).get_queryset()
		rval = rval.filter(user=self.request.user)
		return rval

class EditUser(UpdateView):
	template_name = 'USER/editprofile.html'
	model = cUser
	form_class = cUserChangeForm
	def get(self,request):
		if request.user.is_authenticated():
			if request.user.is_superuser:
				return mod_response(redirect(reverse('Home:home')))
			return mod_response(super(EditUser,self).get(self,request))
		return mod_response(redirect(reverse('Home:home')))

	def get_success_url(self):
		return reverse('USER:profile')

	def get_object(self, queryset=None):
		return self.request.user

class DeleteUser(DeleteView):
	model = cUser
	template_name = "Product/delitem.html"
	def get(self,request):
		if request.user.is_authenticated():
			if request.user.is_superuser:
				return mod_response(redirect(reverse('Home:home')))
			return mod_response(super(DeleteUser,self).get(self,request))
		return mod_response(redirect(reverse('Home:home')))

	def post(self,request):
		user = request.user
		if user.is_authenticated():
			user.is_active = False
			user.save()
			return mod_response(redirect(reverse('USER:logout')))
		return mod_response(redirect(reverse('Home:home')))

	def get_success_url(self):
		return '/'

	def get_object(self, queryset=None):
		return self.request.user

class ChangePasswordUser(View):
	template_name = 'USER/changepass.html'
	def get(self,request):
		if request.user.is_authenticated():
			if request.user.is_superuser:
				return mod_response(redirect(reverse('Home:home')))
			form = PasswordChangeForm(request.user)
			return mod_response(render(request,self.template_name, {'form': form}))
		return mod_response(redirect(reverse('Home:home')))

	def post(self,request):
		if request.user.is_authenticated():
			form = PasswordChangeForm(request.user,request.POST)
			if form.is_valid():
				user = form.save()
				update_session_auth_hash(request, user)
				return mod_response(redirect(reverse('USER:profile')))
		return mod_response(redirect(reverse('USER:changepassword')))

def activation(request,key):
	obj = ActivationKey.objects.filter(hash_key=key).first()
	if obj:
		u = obj.user
		u.is_active = True
		obj.delete()
		u.save()
		return mod_response(redirect('activate_success'))
	return mod_response(redirect(reverse('Home:home')))





