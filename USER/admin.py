from django.contrib import admin

# Register your models here.
# from django.contrib.auth import get_user_model
from django.contrib.auth.admin import UserAdmin

from .forms import cUserCreationForm, cUserChangeForm
from .models import cUser, ActivationKey

class cUserAdmin(UserAdmin):
    add_form = cUserCreationForm
    form = cUserChangeForm
    model = cUser
    list_display = ['first_name','email', 'username','last_name','is_active','is_staff',]

admin.site.register(cUser, cUserAdmin)
admin.site.register(ActivationKey)


