# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2018-10-10 14:07
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('Product', '0002_product_user'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='product',
            name='subcategory',
        ),
        migrations.AddField(
            model_name='product',
            name='subcategory',
            field=models.ForeignKey(default='0', on_delete=django.db.models.deletion.CASCADE, to='Product.SubCategory'),
            preserve_default=False,
        ),
    ]
