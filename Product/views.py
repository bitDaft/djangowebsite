from django.shortcuts import render, redirect
from django.views.generic import CreateView, UpdateView, DeleteView, DetailView, ListView, View

from .models import Category, SubCategory, Product
from .forms import CatForm, SubCatForm, ProdNewForm 
from django.core.urlresolvers import reverse
from emails.forms import MailsForm

# Create your views here.
# t checklist
# t creation
# t editing
# t deleting
# t listing multiple
# t listing single details
# f payment 

# TODO change success urls to reverse
# check permissions working and test cases for general use cases
# and also edge test cases if it exist 

def mod_response(response):
    response["Cache-Control"] = "no-cache, no-store, must-revalidate" # HTTP 1.1.
    response["Pragma"] = "no-cache" # HTTP 1.0.
    response["Expires"] = "0" # Proxies.
    return response


# used to create new products by the user
class NewItem(CreateView):
    model = Product
    form_class = ProdNewForm    
    template_name = 'Product/additem.html'

    def get(self,request):
        if request.user.is_authenticated():
            return mod_response(super(NewItem,self).get(self,request))
        return mod_response(redirect(reverse('Home:home')))
    
    def post(self,request):
        rval = super(NewItem,self).post(self,request)
        if self.object:
            self.object.user = request.user
            self.object.save()
        return mod_response(rval)
        
    def get_success_url(self):
        return reverse('Product:additem')

# ajax request sub category
def load_subcat(request):
    cat_id = request.GET.get('category_id')
    aa = Category.objects.get(id=int(cat_id))
    subcatlist = SubCategory.objects.filter(category=aa)#.order_by('subcategory_name')
    return render(request, 'Product/productajax.html', {'subcatlist': subcatlist})
    
# used to edit the details of the product by the user
class EditItem(UpdateView):
    template_name = 'Product/additem.html'
    model = Product
    form_class = ProdNewForm
    extra_context = {"work":"edit"}
	
    def get(self,request,pk=-1):
        user = request.user
        pk = int(pk)
        if user.is_authenticated() and pk > 0 :
            if self.model.objects.filter(pk=pk,user=user).first():
                return mod_response(super(EditItem,self).get(self,request))
        return mod_response(redirect(reverse('Home:home')))

    def get_success_url(self):
        return reverse('USER:profile')


	def get_object(self, queryset=None):
		pk = int(self.kwargs['pk'])
		return self.model.objects.get(pk=pk) 


# used to delete the product by a user
class DeleteItem(DeleteView):
    model = Product
    template_name = "Product/delitem.html"

    def get(self,request,pk=-1):
        user = request.user
        pk = int(pk)
        if user.is_authenticated() and pk > 0 :
                if self.model.objects.filter(id=pk).first():
                    return mod_response(super(DeleteItem,self).get(self,request))
        return mod_response(redirect(reverse('Home:home')))

    def get_success_url(self):
        return reverse('USER:profile')

    def get_object(self, queryset=None):
        pk = int(self.kwargs['pk'])
        return self.model.objects.get(id=pk)
    
# used for generic searching the product listing
class ListItemSearch(ListView):
    # add option for sorting the list
    model = Product
    template_name = 'Product/search.html'
    context_object_name = 'object'
    def get(self,request):
        # query = request.GET.get('qry')
        return mod_response(super(ListItemSearch,self).get(self,request))
        # return mod_response(redirect(reverse('Home:home')))
    
    def get_queryset(self):
        rval = super(ListItemSearch,self).get_queryset()
        qry = self.request.GET.get('qry')
        if qry :
            rval = rval.filter(product_name__icontains=qry,product_is_approved=True)
        if not len(rval):
            return None
        return rval
        
    def get_context_data(self,**kwargs):
        t = super(ListItemSearch,self).get_context_data(**kwargs)
        t["query"] = self.request.GET.get('qry')
        return t
    
# used for showing the product page with addtional information
class DetailItem(DetailView):
    model = Product
    template_name = 'Product/det.html'
    def get(self,request,pk=-1):
        if pk > 0:
            return mod_response(super(DetailItem,self).get(self,request))
        return mod_response(redirect(reverse('Home:home')))

    def get_context_data(self,**kwargs):
        t = super(DetailItem,self).get_context_data(**kwargs)
        print(t,")"*30)
        t["contact"] = MailsForm()
        return t

    def post(self,request):
        request.session['yes_mail'] = True
        request.session['mail_type'] = 0
        request.session['reciever'] = self.object.user.email
        request.session['subject'] = "[LootMarket] Contact by user about product" + self.object.product_name
        request.session['sender'] = request.POST.get('sender')
        request.session['message'] = request.POST.get('message')
        return mod_response(redirect(reverse('emails:newmail')))




