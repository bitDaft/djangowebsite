from django.conf.urls import include, url
from django.contrib.auth import views as auth_views
from django.views.generic import TemplateView
from .views import NewItem, EditItem, DeleteItem, DetailItem, ListItemSearch, load_subcat


urlpatterns = [
    # Examples:
    # url(r'^$', 'ShoppingCart.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^additem/$',NewItem.as_view(),name="additem"),
    # url(r'^additem/',NewItem.as_view()),

    url(r'^edititem/(?P<pk>\d+)/$',EditItem.as_view(),name="edititem"),
    # url(r'^edititem/',EditItem.as_view()),

    url(r'^deleteitem/(?P<pk>\d+)/$',DeleteItem.as_view(),name="deleteitem"),
    # url(r'^deleteitem/',DeleteItem.as_view()),

    # url(r'^listitem/$',ListItem.as_view(),name="listitem"),
    # url(r'^listitem/',ListItem.as_view()),
    url(r'^listitemsearch/',ListItemSearch.as_view(),name="listitemsearch"),
    # url(r'^listall/',ListItemAll.as_view(),name="listall"),

    url(r'^viewitem/(?P<pk>\d+)/$',DetailItem.as_view(),name="viewitem"),
    # url(r'^viewitem/',DetailItem.as_view()),


    url(r'^ajax/load-cities/$', load_subcat, name='ajax_load_subcat'),
]
