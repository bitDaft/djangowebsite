from django import forms 

from .models import Category, SubCategory, Product
from django.forms.widgets import CheckboxSelectMultiple

class CatForm(forms.ModelForm):
	class Meta:
		model = Category
		fields = ['category_name']

class SubCatForm(forms.ModelForm):
	class Meta:
		model = SubCategory
		fields = ['category','subcategory_name']

class ProdNewForm(forms.ModelForm):
	class Meta:
		model = Product
		fields = ['category','subcategory','product_name','product_estimated_cost','product_description','product_image','product_location']
	
	def __init__(self, *args, **kwargs):
		super(ProdNewForm, self).__init__(*args, **kwargs)
		# self.fields["subcategory"].widget = CheckboxSlectMultiple()
		self.fields["subcategory"].queryset = SubCategory.objects.none()
		if 'category' in self.data:
			try:
				cat_id = int(self.data.get('category'))
				self.fields['subcategory'].queryset = SubCategory.objects.filter(category=Category.objects.get(id=cat_id)).order_by('subcategory_name')
			except (ValueError, TypeError):
				pass  # invalid input from the client; ignore and fallback to empty queryset
		elif self.instance.pk:
			self.fields['subcategory'].queryset = self.instance.category.subcategory_set.order_by('subcategory_name')

# class ProdForm(forms.ModelForm):
# 	class Meta:
# 		model = Product
# 		fields = ['category_id','subcategory_id','product_name']











