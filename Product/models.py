from django.db import models

from USER.models import cUser

# Create your models here.
from geoposition.fields import GeopositionField

class Category(models.Model):
	category_name = models.CharField(max_length=255,unique=True)
	def __str__(self):
		return self.category_name

class SubCategory(models.Model):
	category = models.ForeignKey(Category,on_delete=models.CASCADE)
	subcategory_name = models.CharField(max_length=255)
	def __str__(self):
		return self.subcategory_name

class Product(models.Model):
	category = models.ForeignKey(Category)
	subcategory = models.ForeignKey(SubCategory)
	user = models.ForeignKey(cUser,null=True,blank=False,default=None)
	product_name = models.CharField(max_length=255)
	product_estimated_cost = models.CharField(max_length=255)
	product_description = models.TextField(max_length=500)
	product_brochure = models.FileField(upload_to="product_brochure",default=" ",blank=True)
	product_image = models.ImageField(upload_to="product_image")

	product_is_approved = models.BooleanField(default=False)
	product_location = GeopositionField(blank=False)

	def __str__(self):
		return self.product_name





