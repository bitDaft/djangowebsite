from django.conf.urls import include, url
from django.contrib import admin

from django.conf.urls.static import static
from django.conf import settings
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.contrib.auth import views as auth_views

# from django.views.generic import TemplateView

urlpatterns = [
    # Examples:
    # url(r'^$', 'Project_Land.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
    # url(r'^paytest/', TemplateView.as_view(template_name="payment.html"),name="pay"),
    url(r'^oauth/', include('social_django.urls',namespace='social')),
    url(r'^admin/', include(admin.site.urls),name="admin"),
    url(r'^', include('Home.urls',namespace="Home")),
    url(r'^', include('USER.urls',namespace="USER")),
    url(r'^', include('Product.urls',namespace="Product")),
    url(r'^', include('emails.urls',namespace="emails")),
    url(r'^', include('Purchase.urls',namespace="Purchase")),
    url(r'^password_reset/$', auth_views.password_reset, name='password_reset'),
    url(r'^password_reset/done/$', auth_views.password_reset_done, name='password_reset_done'),
    url(r'^reset/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
        auth_views.password_reset_confirm, name='password_reset_confirm'),
    url(r'^reset/done/$', auth_views.password_reset_complete, name='password_reset_complete'),
    # url(r'^admin/logout/$', 'django.contrib.auth.views.logout',{'next_page': '/'},name="admlogout"),
]


urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
